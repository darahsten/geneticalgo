/*
 * mainfile.cpp
 *
 *  Created on: Apr 10, 2016
 *      Author: sodara
 */



/*
 * Released on: 6th Apr 2016, Expected completion: 13th Apr 2016, Late submission: 16th April.
 *	 Number of credits: 8
 * Write a program to do the following:
Write a program using genetic algorithms to fit a polynomial function in single variable to input data.
Input will be in the form of a text CSV file. Your code will take as input the CSV file and output
coefficients on the screen. For example, for the
function y = 2x^3+3x^2 +5x +4 Input will be in the file given in
“3_assignment_sample_data.csv”.
The output on screen must be: [2, 3] [3, 2] [5, 1] [4, 0]

Hints:
First implement with the assumption that the coefficients are all +ve. Then think of what you would do when
some of them can be +ve and -ve.
Think about how the population will be encoded.
Consider how mutation and mating will work.
You will lose points if your code cannot accept CSV file as input or if it does not output results in the correct format.
You can use any Matlab, R, or C++. However, no internal libraries will be allowed for the genetic algorithm bit.
Input-output and processing of CSV files using libraries is OK. You should be able to explain your code.
If you want to use any other programming language, please get email permission from instructor or TA.
*/
#include "genetics.h"
using namespace std ;

void printIntro(){
	std::cout<<"GENETIC ALGORITHMS \nAuthor : Stephen Odara \nDate : April 2016 \n";
	std::cout<< "This is part of course work assignment for Pattern Recognition class\n";
	std::cout<< "Professor : Suryaprakash Kompali\n";
	std::cout<< "Copyright : As applicable to Carnegie Mellon University Intellectual Property Laws\n\n\n";
	std::cout<< "NOTES \n";
			std::cout<<	"1. I did alot of optimization on this code in terms of genetic algorithms\n";
			std::cout<<" i. Such as I only accept unique individuals in my population while mating\n";
			std::cout<<" This greatly increases the diversity of my population \n";
			std::cout<<" ii. I also place a very high level of mutation at 50% which also greatly ";
			std::cout<<" increases the diversity of the population\n";
			std::cout<<" iii. The search space is limited to genes in the range of 1- 8 \n";
	std::cout<<"2. Starting the algorithm with a high  population makes the algorithm to converge";
			std::cout<<"very fast, for a population of 32 , convergence is in one iteration \n";
			std::cout<<"I noticed the algorithm can converge if the initial population is 4\n";
			std::cout<<"The population is set in the function genetics.initialise()\n";
			std::cout<<" Very small populations are not guaranteed to converge I recommend atleast 8\n";
			std::cout<<" I DON'T HAVE TIME TO UNDO THESE OPTIMISATIONS AND CUSTOMISATIONS NOW\n";
}
void printUsage(){

	std::cout<<"Usage :\n./genetics <filetosource.csv>\n";
	std::cout<<"Assumptions are: \n1. File is a csv file \n";
	std::cout<<"2.The file has format below \n";
	std::cout<<"THE Data is in the format below \n";
	std::cout<<"coeff: bn, bn-1, bn-2, b0\nsample data: \nx, y\n";
	std::cout<<"x1, y1 \nx2, y2\nx3, y3\nx4, y5\n.., ..\n.., ..\nxn, yn\n\n";
	std::cout<<"NOTE : PROGRAM IS WRITEEN FOR FUNCTION WITH 4 COEFFICIENTSi.e n=4\n\n";
}

int main(int argc, char **argv){
	printIntro();
	if (argc<2){
		printUsage();
		return -1;
	}else{
		std::string filename = argv[1];
		genetics genealgo(filename, 8, 0.15);//the middle is the initial population
		//testing load cvs
		genealgo.loadcsv();
		std::cout<<" Finished loading the file \n Now running the algorithm \n";

		genealgo.run_ga();

	}
}
