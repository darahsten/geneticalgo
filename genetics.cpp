#include <cstdlib>
#include <fstream>
#include <sstream>
#include "genetics.h"
#include<ctime>

//Stack over flow for the trim functions
//http://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
// trim from start (in place)
static inline void ltrim(std::string &s) {
	s.erase(s.begin(),
			std::find_if(s.begin(), s.end(),
					std::not1(std::ptr_fun<int, int>(std::isspace))));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
	s.erase(
			std::find_if(s.rbegin(), s.rend(),
					std::not1(std::ptr_fun<int, int>(std::isspace))).base(),
			s.end());
}

//another set  of global methods
double bintodec(std::string inString){
	 double decimal = std:: bitset <8>(inString).to_ulong();
	 return decimal;
}

std::string dectobin(double valIn){
	std::string binval=std:: bitset <8>(valIn).to_string();
	return binval;
}

void testfit(){
	//function y = 2x^3+3x^2 +5x +4\n
	double mydoubles [] ={2, 3, 5, 4};
	std::cout<<" Testing the fitness function\n";
	std::vector<genes> coeffs;
	gene gin[] = {genea, geneb, genec, gened};
	for(int i=0; i<4; i++){
		genes gn(gin[i], dectobin(mydoubles[i]));
		coeffs.push_back(gn);
	}
	double val=fitnesscalc::fitnessfunc(&coeffs);
	std::cout<<" the returned val is "<<val<<std::endl;

}
// trim from both ends (in place)
static inline void trim(std::string &s) {
	ltrim(s);
	rtrim(s);
}


/*class genes{
 genes();
	genes(gene ch, double value);
	virtual ~genes();
	gene getgene();
	void setgene(gene ch);
	void addnoise(double noise);
	std::string getstringvalue();
	double getvalue();
	void addmutations();
	double bintodec(std::string bin);
	void setvalue(double value);
	void setstringvalue(std::string val);
 };*/

/*Returns a binary string of the integers 1 to 9
 * Assumes that srand has been seeded already ...*/
std::string getrandomstring(){
	int randval=rand()%10; //rand string of 1 to 9 ...
	return dectobin(randval);
}
genes::genes() {
	genename = genea;
	genevalue = getrandomstring();
}

void genes::addmutations(){
	int randbin= rand()%2;//
	int loc= rand()%3+1;//map it to location 1,2,3 in binval where changes are made
	std::string modifiedbin="";
	/*std::cout<<" performing mutations ";
	std::cout<<" at location "<<loc<<std::endl;
	std::cout<<" old gene value " <<genevalue<<std::endl;*/
	std::string trunk = genevalue.substr(0, 5);
	std::string binval[4];
	binval[0]= trunk;
	binval[1]=genevalue.substr(5,1);
	binval[2]=genevalue.substr(6,1);
	binval[3]=genevalue.substr(7,1);
	/*for(int i=0; i<4; i++)
		std::cout<<" The val of bin at "<<binval[i]<<std::endl;*/
	std::string valtochange = binval[loc];
	if(valtochange.find("0")!=-1){
		binval[loc]="1";
	}else{
		binval[loc]="0";
	}

	for(int i=0; i<4; i++){
		modifiedbin=modifiedbin+binval[i];
	}
	genevalue=modifiedbin;
	/*std::cout<<" Modified bin val "<<modifiedbin<<std::endl;
	std::cout<<" The new mutated gene is "<<genevalue<<std::endl;*/


}
double genes::bintodec(std::string inString){
	 double decimal = std:: bitset <8>(inString).to_ulong();
	 return decimal;
}

std::string genes::dectobin(double valIn){
	std::string binval=std:: bitset <8>(valIn).to_string();
	return binval;
}
genes::genes(gene ch, std::string value) {
	genename = ch;
	genevalue = value;
}

void genes::setgenename(gene ch) {
	genename = ch;
}

gene genes::getgenename() {
	return genename;
}

//this effectively constrains my solution to a value somewhere btn 1 n 6
double genes::genemax = 6.0;
double genes::genemin = 1.0;

void genes::setvalue(double val) {
	genevalue = dectobin(val);
}
std::string genes::getstringvalue(){
	return genevalue;
}

void genes::setvalue(std::string val){
	genevalue = val;
}

double genes::getvalue(){
	return bintodec(genevalue);
}

genes::~genes() {
	//the genes class of genes ...
}

//fitness calculator
fitnesscalc::fitnesscalc() {
}

std::vector<double> fitnesscalc::true_estimators;
std::vector<double> fitnesscalc::x_val;
std::vector<double> fitnesscalc::powers;

double fitnesscalc::fitnessfunc(std::vector<genes> *coeffs) {
	int numentries = fitnesscalc::x_val.size();
	double error;
	double sumerror=0;
	double estimate=0;
	double true_val;
	std::vector<double> xvals=fitnesscalc::x_val;//this is a shortener so is the next ...
	std::vector<double> pwrs=fitnesscalc::powers;

	/*
	 * TODO I determined that a reading inserts row with zero x and zero y
	 * leaving the estimate to be 4 (+4). This has been messing me up for
	 * over an hour ... Completely unable to see how this came up
	 * Ignoring the first value should solve it for now*/
	for (int i = 1; i < numentries; i++) {
		true_val= fitnesscalc::true_estimators.at(i);
		estimate = coeffs->at(0).getvalue()*(std::pow(xvals.at(i), pwrs.at(0)))+
				coeffs->at(1).getvalue()*(std::pow(xvals.at(i), pwrs.at(1)))+
				coeffs->at(2).getvalue()*(std::pow(xvals.at(i), pwrs.at(2)))+
				coeffs->at(3).getvalue()*(std::pow(xvals.at(i), pwrs.at(3)));
		error=estimate-true_val;
		sumerror=sumerror+error*error;
		//std::cout<<" The sum of the error so far "<<sumerror<<std::endl;
	}
	return sqrt(sumerror);
}

/* INDIVIDUAL DEFINITIONS */
individual::individual() {
	initialise();
}
/*individual::individual(){
 initialise();
 }*/
void individual::printgenes() {
	std::cout << " Genes :";
	for (int i = 0; i < individualgenes.size(); i++) {
		genes gen = individualgenes.at(i);
		std::cout << "(" << i << " , " << gen.getvalue() << ") ";

	}
	std::cout << "F : " << fitness; //std::endl;
	std::cout << std::endl;
}
individual::~individual() {

}

void individual::initialise() {
	gene numind = genecount;
	gene genepool[] = { genea, geneb, genec, gened };
	//unsigned long t = time(0);
	//srand(t);//::seed48();
	//std::cout<< " Time stamp "<<t<<std::endl;
	int rvalue;
	for (int i = 0; i < 4; i++) {
		rvalue = rand()%8 + 1; //generates a random integer btn 1 and 8 inclusive

		genes ingene(genepool[i], dectobin(rvalue));
		individualgenes.push_back(ingene);
	}
}

std::string individual::getindividkey(){
	std::string key="";
	for(int i=0; i<individualgenes.size(); i++){
		key = key+individualgenes.at(i).getstringvalue();
	}
	return key;
}

/*
 * Adds random mutations if a random value returns an int less than mutation rate which is a 15 based value
 * This value is fized now at 15%
 */
void individual::addmutations() {
	//unsigned long tt = time(0);
	//srand(tt);//::seed48();

	double randx;
	int randbin = rand()%2; //a rand binary 0 or 1
	int location = rand()%individualgenes.size(); //a rand value btn 0 and 3 . The chosen rand int will be inserted
	int rate = rand()%100; //will give a value from 0 to 99
	if(rate < 80){// a very high mutation rate to see what happens
		//std::cout<<" Before Mutations "<<std::endl;
		//printgenes();
		genes genetochange = individualgenes.at(location);
		genetochange.addmutations();
		individualgenes.at(location)=genetochange;//not sure of what happens previously
		//std::cout<<" After Mutations "<<std::endl;
		//printgenes();
	}
}

double individual::getfitness() {
	return fitness;
}
void individual::setgene(genes ingene) {
	gene ggene = ingene.getgenename();	 //the gene name e.g gene a ...
	//std::string value = ingene.getstringvalue();
	//ingene.setgenevalue(value);	//force it to become a double .. will that result in better results I wonder ..
	individualgenes.at(ggene) = ingene;
}

genes individual::getgene_at(int index) {
	gene mygenes[] = { genea, geneb, genec, gened };
	if (index >= 0 && index < 4)
		return individualgenes.at(mygenes[index]);
	else
		return genes(mygenes[0], "000");
}

bool individual::issame(individual &other) {
	for (int i = 0; i < individualgenes.size(); i++) {
		if (individualgenes.at(i).getvalue()
				!= (other.getgene_at(i).getvalue())) {
			return false; //early fail
		}
	}
	return true;
}
//I have sufferred here so much ...
void individual::mate(individual *partner, std::map<std::string, individual> *children_map){
	//wrong_mem is a key map helps to reject any members in the system
	int loops = 0;
	double avfit = (getfitness() + partner->getfitness()) / 2;
	//std::cout<<" Average fitness "<<avfit<<std::endl; int numadded;
	int randint;
	int genesize = individualgenes.size();
	int numadded = 0;
	//std::cout<<" We are inside the mate for child once \n";
	while (numadded < 4) {
		genes gen[genesize];
		for (int i = 0; i < genesize; i++) {
			randint =rand() % 2;//either a zero or a 1
			//std::cout<<" The random dint "<<randint<<" Equal opportunity of going anywhere \n";
			if (randint == 0) {
				gen[i] = partner->getgenes().at(i);
			} else {
				gen[i] = individualgenes.at(i);
			}
		}

		individual child;
		for (int i = 0; i < genesize; i++) {
			child.setgene(gen[i]);
		}
		child.addmutations();
		for(int i=0; i<child.getgenes().size(); i++){
			if(child.getgene_at(i).getvalue()>8 ||
					child.getgene_at(i).getvalue()<1){
				child.setgene(individualgenes.at(i));
			}
		}

		child.calcfitness(fitnesscalc::fitnessfunc);
		/*I was having problems with generations actually ending up poorer than the parents*/
		//std::cout<<" Average fitness "<<avfit<<std::endl;

		std::pair<std::string, individual> insertpair=std::make_pair(child.getindividkey(), child);
		std::pair<std::map<std::string,individual>::iterator, bool> inserted=children_map->insert(insertpair);

		if(inserted.second){
			numadded++;
			loops++;//we inserted
		}else{
			loops++;
		}
		if (loops == 200) {
			break; //a very big number just get out
		}
	}
}

double getrandombtn(double min, double max) {
	//std::cout<<" min "<<min<< " max "<<max<<std::endl;
	/*unsigned int tt =std::time(NULL);
	 srand(tt);*/
	double ret = (max - min) * ((double) std::rand() / (double) RAND_MAX) + min;
	//std::cout<<" Return value "<<ret<<std::endl;
	return ret;
}

double individual::getrandom() {
	return (genes::genemax - genes::genemin)
			* ((double) std::rand() / (double) RAND_MAX) + genes::genemin;
}

void individual::calcfitness(double (*fitnessfunction)(std::vector<genes>*)) {
	fitness = fitnessfunction(&individualgenes);
}

genetics::genetics(std::string path, int nump, double rate) {
	filepath = path;
	populationsize = nump;
	mutation_rate = rate;
	std::cout
			<< " &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& \n\n\n";
	initialise(); //initialises our population
	std::cout
			<< " ############################################################### \n\n\n";
	//TODO u_t_o-generated constructor stub
}

/********************************
 * initializes a population
 *******************************/
void genetics::initialise() {
	std::cout
			<< " ============================================================== \n";
	; //=std::time(NULL);
	for (int i = 0; i < populationsize; i++) {
		unsigned int tt = std::time(NULL);
		std::cout << " Current time stamp " << tt << std::endl;
		individual ind;
		ind.printgenes();
		population.push_back(ind);
	}
}

std::vector<genes> individual::getgenes() {
	return individualgenes;
}
void genetics::evaluate(std::vector<individual> *mem) {
	for (int i = 0; i < mem->size(); i++) {
		mem->at(i).calcfitness(fitnesscalc::fitnessfunc);
	}
}

void genetics::select(std::vector<individual> *mem,
		std::vector<individual> *selected) {
	if (mem->size() < selected->size()) {
		std::cerr << " Sorry the size of the mem is too small :";
		std::cout << "Mem size " << mem->size() << " Selected size "
				<< selected->size() << std::endl;
		return;
	}
	std::sort(selected->begin(), selected->end()); //just ensure everything is okay
	std::vector<individual> individ;
	individ.push_back(mem->at(0));
	for (int i = 1; i < mem->size(); i++) {
		if (mem->at(i).issame(mem->at(i - 1))) {
			//ski[
		} else {
			individ.push_back(mem->at(i));
		}
	}
	//std::cout<<" The Original size : "<<mem->size()<<" The final size "<<individ.size()<<std::endl;

	for (int i = 0; i < population.size(); i++) {
		selected->push_back(individ.at(i)); //we dont need any more inefficient systems.
	}
}

//this is a quick hack to do the job doesn't work
bool genetics::ischarx_y(std::string *line) {
	if (line->find("x") != -1 && line->find("y") != -1)
		return true;
	else
		return false;
}

void genetics::getcoeffs(std::string *line, std::vector<double> *coeffs) {
	std::string rem; //the remainder of the string ...
	std::cout << "Getting Coefficients from file" << line << std::endl;
	int colonpos = line->find(":");
	rem = line->substr(colonpos + 1); //the rest of the infor
	int coma = coma = rem.find(",");

	//start by getting the first
	std::string firstv = rem.substr(0, coma);
	trim(firstv);
	coeffs->push_back(atof(firstv.c_str()));

	while (coma != -1) {
		//std::cout<<"The remaining line "<<rem<<std::endl;
		//std::cout<<"coma pos "<<coma<<std::endl;
		std::string val = rem.substr(coma + 1, coma + 2);
		//std::cout<<" The value of val "<<val<<std::endl;
		trim(val);
		coeffs->push_back(atof(val.c_str()));
		rem = rem.substr(coma + 1); //the rest of the infor
		coma = rem.find(",");
	}

	for (int i = 0; i < coeffs->size(); i++) {
		std::cout << " The value of coeff at " << i << " is " << coeffs->at(i)
				<< "\n";
	}
}

void genetics::splitlines(std::string *lines,
		std::pair<double, double> *values) {
//Assumes the line is doublea, doubleb
	int comapos = lines->find(",");
	std::string first = lines->substr(0, comapos);
	std::string second = lines->substr(comapos + 1); //until the end simple algorithm
	trim(first);
	trim(second);
	char *end;
	values->first = std::strtod(first.c_str(), &end);
	values->second = std::strtod(second.c_str(), &end);

}

void genetics::loadcsv() {
	//THE DATA FORMAT FROM THE INPUT FILE
	/*coeff: 3, 2, 1, 0
	 sample data:
	 x,y
	 0.6680935218,9.2759201602
	 0.3946681337,6.5635788105
	 0.4793039009,7.3059393001
	 0.1321874769,4.7179775348
	 0.9189982234,12.6809584366
	 0.1946131573,5.1014302961*/
	std::string line;

	std::cout << "The file name/path : " << filepath.c_str() << std::endl;
	trim(filepath);

	std::ifstream src_file(filepath.c_str(), std::ifstream::in); //ensure file path is valid
	if (src_file.good())
		std::cout << " The file is present \n";
	else
		std::cout << " File has not been found \n";
	if (src_file.is_open()) {
		/*what we have
		 * 1. an array of coefficients
		 * 2. the name of input variables
		 * 3. an array of inputs
		 * 4. an array of y values*/
		std::vector<double> coeffs;
		std::vector<double> x; //corresponding to the first value
		std::vector<double> y; //corresponding to the second value
		bool readingdata = false;
		while (getline(src_file, line)) {
			std::cout << " The Line Output" << line << '\n';
			if (line.find("coeff") != -1) {
				getcoeffs(&line, &coeffs);
			} else {
				if (line.find("sample") != -1) {
					std::cout << " WE HAVE SAMPLE DATA \n";
					readingdata = true;
				} else if (line.find(",") == -1) {
					std::cout << " NOT COMMA DELIMITED PROCESSING ENDED \n";
				} else {
					if (readingdata) {
						std::pair<double, double> values;
						splitlines(&line, &values);
						x.push_back(values.first);
						y.push_back(values.second);
					}
					//data is locked to behaving like x n y
					if (ischarx_y(&line))
						readingdata = true;
				}
			}
		}
		src_file.close();
		/*assigning stuff to the algorithm ...*/
		fitnesscalc::powers = coeffs;
		fitnesscalc::x_val = x;
		fitnesscalc::true_estimators = y;
	} else {
		std::cerr << "FILE IS CLOSED WE HAVE A PROBLEM \n";
		return;
	}

	//assign data to fitness params
}

bool genetics::checkconvergence(std::vector<individual> *selected) {
	double indfit = selected->at(0).getfitness();
	//std::cout<<" The individual is selected has fitness "<<indfit<<std::endl;
	if (indfit > 0.1)
		return false;
	return true;
}

void genetics::mate(std::map<std::string, individual> *children) {
	int numchild = children->size(); //int numrun;
	//std::cout<<"\n\n===================================================================== \n";
	//std::cout<<" The size of the population is "<<population.size()<<std::endl;
	/*for(int i=0; i<4; i++){
	 population.at(i).printgenes();
	 }*/
	//std::cout<<" We are mating the genes \n";
	for (int i = 0; i < populationsize; i++) {
		for (int j = 0; j < populationsize; j++) {
			population.at(i).mate(&population.at(j), children);
			//std::cout<<"=";
		}
	}
	numchild = children->size();
	//std::cout<<"\n\n********************************************************************* \n";
	//std::cout<<" The size of children : "<<children->size()<<std::endl;
	 /*for(int i=0; i<4; i++){
	 children->at(i).printgenes();
	 }*/

}
void genetics::getchildren(std::vector<individual> *children,
		std::map<std::string, individual> *children_map){
	 	 std::map<std::string,individual>::iterator it = children_map->begin();
	 	for (it=children_map->begin(); it!=children_map->end(); ++it)
	 	     children->push_back(it->second);
}
void genetics::run_ga() {
	/*I really wonder if this works for the global thing
	 * The problem is that during the same run if i call srand I usually end up with the same
	 * sequence because it seems to use the same seeds due to static casting*/
	srand(static_cast<unsigned>(time(0)));
	bool converged = false;
	int numgenerations = 0;
	while (!converged) {
		std::vector<individual> children;
		std::map<std::string, individual> children_map; // I use this as a hack to ensure i have unique children
		std::vector<individual> selected;
		evaluate(&population);
		mate(&children_map);
		getchildren(&children, &children_map);//loads from children map to children
		if (children.size() < population.size()) {
					std::cout << " The size of children is " << children.size()
							<< std::endl;
					std::cout
							<< " FORCED CONVERGENCE CHILDREN SIZE IS LESS THAN POPULATION \n";
					break;
				}
		evaluate(&children);
		std::sort(children.begin(), children.end());
		select(&children, &selected);
		if (numgenerations % 20 == 0) {
			std::cout << "\n\n\nPrinting the top 2 mem population info for generation " << numgenerations
					<< std::endl;
			for (int i = 0; i < 2; i++) {
				population.at(i).printgenes();
			}

			std::cout << "\n\n\nPrinting the top 2 selected children for generation "<<numgenerations<<std::endl;
			for (int i = 0; i < 2; i++) {
				selected.at(i).printgenes();
			}
		}
		converged = checkconvergence(&selected);
		numgenerations++;
		if (converged) {
			population=selected;
			std::cout << " WE HAVE CONVERGED AT GENERATION : "<<numgenerations<<std::endl;
			break;
		}
		population = selected;
	}
	std::vector<genes> sol = population.at(0).getgenes();
	std::cout << " ===== The best solution is ======\n";
	for (int i = 0; i < sol.size(); i++) {
		std::cout << " (" << sol.at(i).getvalue() << "," << fitnesscalc::powers.at(i)
				<< ") ";
	}
	std::cout << std::endl;
}

genetics::~genetics() {

}

bool operator<(const individual &lhs, const individual &rhs) {//individual & lfh,
	return lhs.fitness < rhs.fitness;
}

/**
 * TODO
 * Implement converged
 * 2. Implement readcsv
 * 3 Implement fitnessfunction
 * 4. test the function.
 * 5. Expand the algorithm to include more functions ...
 */
