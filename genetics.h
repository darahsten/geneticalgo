/*
 * genetics.h
 *
 *  Created on: Apr 10, 2016
 *      Author: sodara
 */

#ifndef GENETICS_H_
#define GENETICS_H_
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <cmath>
#include <map>
#include  <bitset>
#include <functional>
#include <stdexcept>

//enum used as array index ... helps to keep clean the
//programming ...
enum gene{
	genea=0, geneb, genec, gened, genecount
};

class genes{
private:
	std::string genevalue;
	gene genename;
public:
	static double genemax;
	static double genemin;
	genes();
	genes(gene ch, std::string value);
	virtual ~genes();
	gene getgenename();
	void setgenename(gene ch);
	std::string getstringvalue();
	double getvalue();
	void addmutations();
	double bintodec(std::string bin);
	std::string dectobin(double valIn);
	void setvalue(double value);
	void setvalue(std::string val);

};


class fitnesscalc{
public:
	//function returns the estimated y value
	static double fitnessfunc(std::vector<genes> *coeffs);//x is the function
	static std::vector<double> powers;
	static std::vector<double> x_val;
	static std::vector<double> true_estimators;
	fitnesscalc();
	~fitnesscalc();
};

class individual{
private:
	std::vector<genes> individualgenes; //genes individualsgenes ;//a gene with four members  ...
	double getrandom();
	void initialise();

/*
 * This ignores the cross over which takes place to further randomise the
 * gene pool*/
public:
	double fitness;
	individual();
	individual(unsigned short timestamp);//to force initialise at random;
	void addmutations();
	void addposmutations(int i);
	void addnegmutations(int i);
	void smoothgenes();
	bool issame(individual &other);
	friend bool operator<(const individual &lhs,const individual &rhs) ;
	void setgene(genes gen);
	void printgenes();
	genes getgene_at(int index);
	std::vector<genes> getgenes();
	double getfitness();
	std::string getindividkey(); //gets string which is a combination of its gene values for use as a map
	/*returns individuals fitness, should be a y value */
	/* obtains from the children map */
	void getchildren(std::vector<individual>*children, std::map<std::string, individual> *chidren_map);
	void mate(individual *partner, std::map<std::string, individual> *children_map);
	void calcfitness(double (*fitnessfunction)(std::vector<genes>*));//takes in a gene array and calculate the function
	virtual ~individual();
};
//bool operator<(const individual lhs,const individual rhs);
/*class does the genetic algorithms*/
class genetics {
private:
	std::vector<individual> population;
	std::map<std::string, individual> wrong_mems;//wrong members of the population
	int populationsize;
	std::string filepath;
	double mutation_rate;
	bool ischarx_y(std::string *line);
	void getcoeffs(std::string *line, std::vector<double> *coeffs);
	void splitlines(std::string *lines, std::pair<double, double> *values);
public:
	/**/
	void initialise();
	void loadcsv();
	void getchildren(std::vector<individual> *child, std::map<std::string,individual> *children_map);
	void evaluate(std::vector<individual> *mem);//evaluates the population using the fitness function
	void select(std::vector<individual> *mem, std::vector<individual> *selected); //selects the best algorithms
	void addmutations(std::vector<individual> *children);
	void mate(std::map<std::string,individual> *children_map);
	bool checkconvergence(std::vector<individual> *selected);
	void run_ga();
	genetics();
	genetics(std::string filepath, int nump = 8, double rate =0.015);
	virtual ~genetics();
};



#endif /* GENETICS_H_ */
